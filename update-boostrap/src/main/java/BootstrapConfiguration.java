import org.update4j.Configuration;
import org.update4j.FileMetadata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.String.format;

public class BootstrapConfiguration {

    public static void main(final String[] args){
        final var applicationVersion = args[0];
        final var jobId = System.getenv("CI_JOB_ID");
        final var configuration = Configuration.builder()
                .baseUri(format("https://gitlab.com/sbailleul/update-demon/-/jobs/%s/artifacts/raw", jobId))
                .basePath("${user.home}/.update")
                .property("default.launcher.main.class", "fr.esgi.update.application.HelloWorld")
                .file(FileMetadata
                        .readFrom(format("update-application/target/update-application-%s", applicationVersion))
                        .path("update.jar")
                        .classpath()
                ).build();
        try {
            Files.writeString(Path.of("update-boostrap/target/update.xml"), configuration.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
